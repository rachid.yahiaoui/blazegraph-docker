#!/bin/bash
    
    export TERM=xterm
    
    EXIT() {
       if [ $PPID = 0 ] ; then exit ; fi
       parent_script=`ps -ocommand= -p $PPID | awk -F/ '{print $NF}' | awk '{print $1}'`
       if [ $parent_script = "bash" ] ; then
           echo; echo -e " \e[90m exited by : $0 \e[39m " ; echo
           exit 2
       else
           echo ; echo -e " \e[90m exited by : $0 \e[39m " ; echo
           kill -9 `ps --pid $$ -oppid=`;
           exit 2
       fi
    }
    
    releasePort() {
      PORT=$1
      echo " Try release port : $PORT "
      if ! lsof -i:$PORT &> /dev/null
      then
        isFree="true"
        echo " Port [[ $PORT ]] Already Released"
      else    
        echo " Port Already in Use. Try release port : $PORT "
        fuser -k $PORT/tcp  &> /dev/null
        sleep 0.1
      fi
    }  
     
     
     while [[ "$#" > "0" ]] ; do
     case $1 in
         (*=*) KEY=${1%%=*}
               VALUE=${1#*=}
               case "$KEY" in
               
                    ("namespace")            NAMESPACE=$VALUE
                    ;; 
                    ("namespaces")           NAMESPACES=$VALUE
                    ;;
                    ("mode")                 RW_MODE=$VALUE
		            ;;
                    ("port")                 PORT=$VALUE
                    ;;
                    ("rw_port")              ReadWritePort=$VALUE
                    ;;
                    ("ro_port")              ReadOnlyPort=$VALUE
		            ;;
		            ("xms")                  XMS=$VALUE
		            ;;
		            ("xmx")                  XMX=$VALUE
		            ;;
		            ("maxDirectMemorySize")  MaxDirectMemorySize=$VALUE
		            ;;	
               esac
         ;;
         help)  echo
	            echo " Total Arguments : Five ( One optionnal )                                        "
                echo
                echo "   mode=                :  rw / ro  ( read_write / read_only )                   "
                echo "   namespace=           :  Blazegraph_namespace                                  "
                echo "   namespaces=          :  Blazegraph_namespaces. Ex : namespaces=ola,acbb,foret "
                echo "   rw_port=             :  Read Write Port number                                "
                echo "   ro_port=             :  Read Only  Port number                                "
                echo "   port=                :  Used to Override RW_PORT and RO_PORT                  "
                echo "   rmDb                 :  Remove DB if already exists                           "
                echo "   xms=                 :  XMS                                                   "
                echo "   xmx=                 :  XMX                                                   "
                echo "   maxDirectMemorySize= :  maxDirectMemorySize                                   "
                echo
                EXIT ;
        ;;
        rmDb | rmDB | rm_db | RM_DB )  RM_DB="rmDb"
        
     esac
     shift
    done    
    
    CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
  
    cd $CURRENT_PATH
    
    RW_MODE=${RW_MODE:-"rw"}
    RM_DB=${RM_DB:-""}
    
    ReadWritePort=${PORT:-${ReadWritePort:-${ReadOnlyPort:-"7777"}}}
    ReadOnlyPort=${PORT:-${ReadOnlyPort:-${ReadWritePort:-"8888"}}}
   
    XMS="-Xms"${XMX:-"12"}"g"
    XMX="-Xmx"${XMX:-"12"}"g"
    MaxDirectMemorySize=${MaxDirectMemorySize:-"28"}"g"
     
    READ_ONLY_XML_CONF="./conf/blazegraph/owerrideXMl/webWithConfReadOnly.xml"
    
         
    if [ "$RW_MODE" != "ro" ] && [ "$RW_MODE" != "rw" ] ; then 
        
         echo
         if [ "$RW_MODE" = "" ] ; then
           echo -e "\e[91m Missing RW_MODE Argument ( 'rw' - 'ro' ) \e[39m "
         else
           echo -e "\e[91m RW_MODE Argument can only have 'rw' or 'ro' value \e[39m "
         fi 
         EXIT          
    fi 
       
    if [ ! -f $READ_ONLY_XML_CONF ]  ; then
       echo
       echo -e "\e[91m Missing $READ_ONLY_XML_CONF ! \e[39m "
       EXIT
    fi
        
         
    BLAZEGRAPH_PATH="./bin/blazegraph_2_1_6.jar"
        
    DIR_BLZ=$(dirname "${BLAZEGRAPH_PATH}")
         
    tput setaf 2
    echo 
    echo " ########################################  "
    echo " ######## Starting EndPoint #############  "
    echo " ----------------------------------------  "
    echo -e " \e[90m$0                        \e[39m "
   
    echo 
    
    echo -e " ##  MODE                : $RW_MODE             "
    echo -e " ##  RW PORT             : $ReadWritePort       "
    echo -e " ##  RO PORT             : $ReadOnlyPort        "
    echo -e " ##  Xms                 : $XMS                 "
    echo -e " ##  Xmx                 : $XMX                 "
    echo -e " ##  MaxDirectMemorySize : $MaxDirectMemorySize "
    echo
    echo -e " ############################################## "
    echo 
     
    tput setaf 7
    
    echo
        
    releasePort $ReadOnlyPort
    releasePort $ReadOnlyPort
    
    ############################################
    ## RUN CONFIGSCRIPT => CREATES NAMESPACES ##
    ############################################
    
    ./config.sh namespace=$NAMESPACE   \
                namespaces=$NAMESPACES \
                rw_port=$ReadWritePort \
                $RM_DB 
  
    ############################################
    ############################################
        
    if [ "$RW_MODE" = "rw" ] ; then 
          
      sleep 0.1         
     
      if [ ! -f "$DIR_BLZ/logs/gc.txt" ]; then
         mkdir -p "$DIR_BLZ/logs/"
         touch "$DIR_BLZ/logs/gc.txt" 
      fi
          
      java -server -XX:+UseG1GC $XMS $XMX -XX:+UseStringDeduplication               \
           -XX:MaxDirectMemorySize=$MaxDirectMemorySize                             \
           -Xloggc:$DIR_BLZ/logs/gc.txt                                             \
           -verbose:gc -XX:+PrintGCDetails                                          \
           -XX:+PrintGCDateStamps -XX:+PrintGCTimeStamps                            \
           -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=10                      \
           -XX:GCLogFileSize=5M                                                     \
           -server -Dorg.eclipse.jetty.server.Request.maxFormContentSize=2000000000 \
           -Dcom.bigdata.journal.AbstractJournal.file=./data/blazegraph.jnl         \
           -Djetty.port=$ReadWritePort -jar $BLAZEGRAPH_PATH                        &
            
      # Wait for the server to finish booting
      while [[ ! -n "$new_job_started" ]] ; do
           
            new_job_started="$(jobs -n)"
         
            if [ -n "$new_job_started" ];then
               PID=$!       
            else
               PID=
            fi
            sleep 0.1 
      done
            
      # Wait for the Port $ReadWritePort to Open
 
      while ! lsof -i:$ReadWritePort &> /dev/null  ;  do
        
           echo "Wait blazegraph to start... Port : $ReadWritePort "
           sleep 0.2 
      done
        
      sleep 0.1
      
      if [ "$NAMESPACE" != *"kb"* -a "$NAMESPACES" != *"kb"* ]; then
      
          RES=$(curl -X DELETE http://localhost:$ReadWritePort/blazegraph/namespace/kb)
          while [[ ! -n "$RES" ]] ; do
           RES=$(curl -X DELETE http://localhost:$ReadWritePort/blazegraph/namespace/kb)
              sleep 0.1
          done
      fi
  
    # Run On Read Only Port 
        
    elif [ "$RW_MODE" = "ro" ] ; then
         
          sleep 0.1
        
          java -server -XX:+UseG1GC $XMS $XMX -XX:+UseStringDeduplication                                  \
               -XX:MaxDirectMemorySize=$MaxDirectMemorySize                                                \
               -Xloggc:$DIR_BLZ/logs/gc.txt                                                                \
               -verbose:gc -XX:+PrintGCDetails                                                             \
               -XX:+PrintGCDateStamps -XX:+PrintGCTimeStamps                                               \
               -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=10                                         \
               -XX:GCLogFileSize=5M                                                                        \
               -server -Dorg.eclipse.jetty.server.Request.maxFormContentSize=2000000000                    \
               -Dcom.bigdata.journal.AbstractJournal.file=./data/blazegraph.jnl                            \
               -Djetty.overrideWebXml=$READ_ONLY_XML_CONF -Djetty.port=$ReadOnlyPort -jar $BLAZEGRAPH_PATH &
               
          
          # Wait for the server to finish booting
          
          while [[ ! -n "$new_job_started" ]] ; do
           
                new_job_started="$(jobs -n)"
            
                if [ -n "$new_job_started" ];then
                PID=$!       
                else
                PID=
                fi
                sleep 0.1 
          done
            
          # Wait for the Port $ReadOnlyPort to Open
 
          while ! lsof -i:$ReadOnlyPort &> /dev/null  ;  do
        
               echo "Wait blazegraph to start... Port : $ReadOnlyPort "
               sleep 0.2 
          done
            
          sleep 0.1
          
    fi
    
    echo  -e " \e[97m "
     
    #  releasePort $ReadOnlyPort
    #  releasePort $ReadOnlyPort
    
    
    chmod -R 777 ./data

    tail -f /dev/null

