# blazegraph-docker

## ~~ Dockerized Blazegraph ~~ 


### 1. Docker Build

```

docker build -t rac021/blazegraph . ; 

```

### 2. Simple Deployment 

```

docker run --rm -d --name blazegraph   \
                    -p 7777:7777       \
                    rac021/blazegraph  \
                    namespace=ore

```

### 3. Use Multiple Namespaces 

```

docker run --rm -d --name blazegraph                  \
                    -p 7777:7777                      \
                    rac021/blazegraph                 \
                    namespaces=ola,acbb,foret port=7777

```

### 4. Use External Volume ( rmDb = remove dataBase if already exists )

```
docker run --rm -d --name blazegraph                    \
                    -p 7777:7777                        \
                    -v $(pwd)/data:/usr/blazegraph/data \
                    rac021/blazegraph                   \
                    mode=rw namespace=ore port=7777 rmDb

```


### 5. Read-Only Mode ( mode=ro ) :

```
docker run --rm -d --name blazegraph                    \
                    -p 7777:7777                        \
                    -v $(pwd)/data:/usr/blazegraph/data \
                    rac021/blazegraph                   \
                    mode=ro namespaces=ola,acbb,foret port=7777 rmDb

```

