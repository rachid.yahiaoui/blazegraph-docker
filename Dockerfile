
FROM openjdk:8u242-jre-buster

COPY . /usr/blazegraph

RUN  apt-get -y update         && \
     apt-get -y install lsof   && \
     apt-get -y install psmisc
     
WORKDIR /usr/blazegraph

ENTRYPOINT ["/usr/blazegraph/start.sh"]

CMD [""]
