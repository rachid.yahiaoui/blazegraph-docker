#!/bin/bash

####################
## ARGS :    #######
#     ip=
#     namespace=
#     namespaces=
#     rw=
#     ro=
####################
####################

   
 IP_HOST="localhost"
 
 DEFAULT_NAMESPACE="MY_NAMESPACE"

 BLZ_JNL="./data/blazegraph.jnl"
 
 BLAZEGRAPH_PATH="./bin/blazegraph_2_1_6.jar"
 
 
 EXIT() {
   if [ $PPID = 0 ] ; then exit ; fi
   parent_script=`ps -ocommand= -p $PPID | awk -F/ '{print $NF}' | awk '{print $1}'`
   if [ $parent_script = "bash" ] ; then
       echo; echo -e " \e[90m exited by : $0 \e[39m " ; echo
       exit 2
   else
       echo ; echo -e " \e[90m exited by : $0 \e[39m " ; echo
       kill -9 `ps --pid $$ -oppid=`;
       exit 2
   fi
 }
  
 releasePort() {
   PORT=$1
   echo " Try release port : $PORT "
   if ! lsof -i:$PORT &> /dev/null
   then
     isFree="true"
     echo " Port [[ $PORT ]] Already Released"
   else    
     echo " Port Already in Use. Try release port : $PORT "
     fuser -k $PORT/tcp  &> /dev/null
     sleep 0.1
   fi
 }  
    

 createNamespace() {
 
        NAMESPACE=$1 
        
        DATALOADER="$CURRENT_PATH/conf/blazegraph/namespace/dataloader.cp.xml"
 
        if [ -f "$DATALOADER" ] ; then 
            rm "$DATALOADER"
        fi
        
        cp "$CURRENT_PATH/conf/blazegraph/namespace/dataloader.xml" "$DATALOADER"  
    
        sed -i "s/$DEFAULT_NAMESPACE/$NAMESPACE/g" $DATALOADER

        CMD=$(curl -X POST --data-binary @$DATALOADER --header 'Content-Type:application/xml' \
                    http://$IP_HOST:$ReadWritePort/blazegraph/dataloader 2>&1                 )
        
        # test if result curl command contains Failed 
                
        CURL_TRYING=10
        CURL_COUNT=0
        
        while [[ "$CMD" = *"Failed"* ]] && [ $CURL_COUNT -lt $CURL_TRYING  ]; do
        
            sleep 0.1
            
            CMD=$(curl -X POST --data-binary @$DATALOADER --header 'Content-Type:application/xml' \
                        http://$IP_HOST:$ReadWritePort/blazegraph/dataloader 2>&1                 )
            echo
            
            let "CURL_COUNT++"
            
        done
        
        sleep 0.1
       
        rm "$DATALOADER"
        
        echo ; echo
        
        echo -e "\e[92m Namespace [ $NAMESPACE ] Created \e[39m "
        
        sleep 0.1 ; echo
        
        # Test connexion with specified namespace 
        
        PREFIX_ENDPOINT="http://$IP_HOST:$ReadWritePort"
        SUFFIX_ENDPOINT="blazegraph/namespace/$NAMESPACE/sparql"

        ENDPOINT=$PREFIX_ENDPOINT/$SUFFIX_ENDPOINT
        
        check="cat < /dev/null > /dev/tcp/http://$IP/$PORT"
            
        echo ; echo -e " Try connection : $ENDPOINT "
                
        TRYING=50
        
        COUNT=0
                    
        OK=$?
                    
        while [ $OK -ne 0 -a $COUNT -lt $TRYING  ] ; do
                
            timeout 1 bash -c "cat < /dev/null > /dev/tcp/$IP_HOST/$ReadWritePort" 2> /dev/null
                    
            OK=$?
                    
            if [ $COUNT == 0 ] ; then echo ; fi 
                
            if [ $OK == 1 ] ; then 
                    
                echo " .. "
                sleep 0.1
                    
            elif [ $OK != 0 ] ; then 
                
                echo " attempt ( $COUNT ) : Try again.. "
                sleep 0.5
                    
            fi
                
            let "COUNT++"
                
            if [ $COUNT == $TRYING ] ; then
                
                echo
                echo -e "\e[31m ENDPOINT $ENDPOINT Not reachable !! \e[39m"
                echo
                EXIT
                    
            fi
                    
        done
                
        echo " Yeah Connected !! " ; echo
 
 }
 
 
 while [[ "$#" > "0" ]] ; do

     case $1 in
   
         (*=*) KEY=${1%%=*}
         
               VALUE=${1#*=}
               
               case "$KEY" in
               
                    ("namespace")  NAMESPACE=$VALUE
                    ;; 
                    ("namespaces") NAMESPACES=$VALUE
                    ;;
                    ("rw_port")    ReadWritePort=$VALUE
                    ;;
                    ("rmDb")       RM_DB="TRUE"
                    ;;
                    ("rmdb")       RM_DB="TRUE"
                    ;;
                    ("rm-db")      RM_DB="TRUE"
                    ;;
                    ("rmDB")       RM_DB="TRUE"
                    ;;
                    ("rm_db")      RM_DB="TRUE"
                    ;;
                    ("RM_DB")      RM_DB="TRUE"
                    ;;
               esac
         ;;
         
         rmDb | rmDB | rm_db | rm-db | RM_DB ) RM_DB="TRUE" 
         
         ;;
         
         help)  echo
                echo " Total Arguments : Five ( One optionnal )                               "
                echo
                echo "   ip=         :  IP Host                                               "
                echo "   namespace=  :  Blazegraph_namespace                                  "
                echo "   namespaces= :  Blazegraph_namespaces. Ex : namespaces=ola,acbb,foret "
                echo "   rw_port=    :  Local Port number, mode Read-Write                    "
                echo "   rmDb        :  Remove Database if already exists                     "
                echo
                EXIT ;
     esac
     
     shift
 done    
 
 NAMESPACE=${NAMESPACE:-kb}
 ReadWritePort=${ReadWritePort:-7777}
    
 RM_DB=${RM_DB:-FALSE}
 
 CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
 cd $CURRENT_PATH
 
 
 tput setaf 2
 
 echo 
 echo -e " ###################################### "
 echo -e " ######### Build Config ############### "
 echo -e " -------------------------------------- "
 echo -e " \e[90m$0        \e[32m                 "
 echo
 echo -e " Blazegraph version : 2.1.6             "
 echo
 echo
 echo -e " ##  NAMESPACE      : $NAMESPACE        "
 echo -e " ##  NAMESPACES     : $NAMESPACES       "
 echo -e " ##  ReadWritePort  : $ReadWritePort    "
 echo -e " ##  REMOVE DB      : $RM_DB            "
 echo
 echo -e " ###################################### "
 echo 
 
 tput setaf 7
 
 sleep 0.1
  
 if [ ! -e $BLAZEGRAPH_PATH ] ; then
      
      echo
      echo " The JAR [ $BLAZEGRAPH_PATH ] Not Found ! "
      EXIT

 fi
 
 if [ -e $BLZ_JNL -a $RM_DB = "TRUE" ] ; then
      
    echo
    echo -e "\e[91m $BLZ_JNL will be deleted \e[39m "
    echo
    read -n1 -t 0.1 -r -p " Press Enter to continue, Any other Key to abort.. else delete in 0.1 s " key
    echo  
    
    if [ "$key" = '' ] ; then
        
        # NOTHING PRESSED 
        
        rm -f $BLZ_JNL &> /dev/null
        echo " $BLZ_JNL Deleted "
        echo
        
    else
    
        # Anything pressed
        echo
        echo " Script aborted "
        EXIT
    fi
 fi

 java -server -XX:+UseG1GC -Dcom.bigdata.journal.AbstractJournal.file=$BLZ_JNL  \
      -Djetty.port=$ReadWritePort -Dcom.bigdata.rdf.sail.namespace=$NAMESPACE -jar $BLAZEGRAPH_PATH &   
 
 # Wait for the server to finish booting
 
 while [[ ! -n "$new_job_started" ]] ; do

    new_job_started="$(jobs -n)"
 
    if [ -n "$new_job_started" ];then
       PID=$!       
    else
      PID=
    fi
    sleep 0.1 
 
 done 
 
 echo ; echo " Blazegraph ID : [ $PID ] " ; echo 

 sleep 0.1
 
 
 # Wait for the Port $ReadWritePort to Open
 
 while ! lsof -i:$ReadWritePort &> /dev/null  ;  do
 
   echo "Wait blazegraph to start... Port : $ReadWritePort "
   sleep 0.2 
 
 done
 
 echo 
 
 echo "==> Create Namespace : [ $NAMESPACE ] " ; echo

 createNamespace $NAMESPACE
 
 echo 
 
 namespaces=$(echo $NAMESPACES | tr "," "\n")

 for namespace in $namespaces ; do
    
    echo "==> Create Namespace : [ $namespace ] " ; echo

    createNamespace $namespace
    
 done 
 
 DEL_KB_NAMESPACE=$(curl -X DELETE http://$IP_HOST:$ReadWritePort/blazegraph/namespace/kb)
 
 while [[ ! -n "$DEL_KB_NAMESPACE" ]] ; do
     DEL_KB_NAMESPACE=$(curl -X DELETE http://$IP_HOST:$ReadWritePort/blazegraph/namespace/kb)
     sleep 0.1
 done
          
 sleep 0.1
 
 releasePort $ReadWritePort  
 
 echo -e "\e[93m Blazegraph Stopped \e[39m "
 echo
 echo -e "\e[92m Use 11_nano_start_stop.sh script to start-stop Blazegraph \e[39m "
 echo

 sleep 0.1

